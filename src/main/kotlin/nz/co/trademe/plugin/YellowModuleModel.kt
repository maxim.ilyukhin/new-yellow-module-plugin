package nz.co.trademe.plugin

import com.android.tools.idea.gradle.npw.project.GradleAndroidModuleTemplate
import com.android.tools.idea.npw.model.ExistingProjectModelData
import com.android.tools.idea.npw.model.ProjectModelData
import com.android.tools.idea.npw.model.ProjectSyncInvoker
import com.android.tools.idea.npw.module.ModuleModel
import com.android.tools.idea.projectsystem.NamedModuleTemplate
import com.android.tools.idea.wizard.template.BytecodeLevel
import com.android.tools.idea.wizard.template.ModuleTemplateData
import com.android.tools.idea.wizard.template.Recipe
import com.google.wireless.android.sdk.stats.AndroidStudioEvent
import com.google.wireless.android.sdk.stats.AndroidStudioEvent.TemplatesUsage.TemplateComponent
import com.intellij.openapi.project.Project
import nz.co.trademe.plugin.YellowModuleRecipe.Companion.generateYellowModule

class YellowModuleModel(
        projectModelData: ProjectModelData,
        template: NamedModuleTemplate,
        moduleParent: String
) : ModuleModel(
        name = EXAMPLE_MODULE_NAME,
        commandName = COMMAND_NAME,
        projectModelData = projectModelData,
        _template = template,
        moduleParent = moduleParent,
        wizardContext = TemplateComponent.WizardUiContext.NEW_MODULE,
        isLibrary = true,
) {

    override val loggingEvent = AndroidStudioEvent.TemplateRenderer.ANDROID_MODULE

    override val renderer = YellowTemplateRenderer()

    companion object {

        const val EXAMPLE_MODULE_NAME = "yellow-module"
        const val COMMAND_NAME = "New Module"

        fun fromExistingProject(
                project: Project,
                moduleParent: String,
                projectSyncInvoker: ProjectSyncInvoker
        ): YellowModuleModel {
            val projectModelData = ExistingProjectModelData(project, projectSyncInvoker) as ProjectModelData
            val template = GradleAndroidModuleTemplate.createSampleTemplate()
            return YellowModuleModel(projectModelData, template, moduleParent)
        }
    }

    inner class YellowTemplateRenderer : ModuleModel.ModuleTemplateRenderer() {
        override val recipe: Recipe
            get() = {
                generateYellowModule(
                        moduleTemplateData = it as ModuleTemplateData,
                        useKts = false,
                        bytecodeLevel = BytecodeLevel.L8,
                        generateGenericLocalTests = true,
                )
            }
    }
}