package nz.co.trademe.plugin

import com.android.tools.idea.npw.module.recipes.addInstrumentedTests
import com.android.tools.idea.npw.module.recipes.addLocalTests
import com.android.tools.idea.npw.module.recipes.generateManifest
import com.android.tools.idea.npw.module.recipes.gitignore
import com.android.tools.idea.npw.module.recipes.proguardRecipe
import com.android.tools.idea.wizard.template.BytecodeLevel
import com.android.tools.idea.wizard.template.Language
import com.android.tools.idea.wizard.template.ModuleTemplateData
import com.android.tools.idea.wizard.template.RecipeExecutor


class YellowModuleRecipe {

    companion object {
        fun RecipeExecutor.generateYellowModule(
                moduleTemplateData: ModuleTemplateData,
                useKts: Boolean,
                generateGenericLocalTests: Boolean = false,
                bytecodeLevel: BytecodeLevel,
        ) {
            val projectData = moduleTemplateData.projectTemplateData
            val srcOut = moduleTemplateData.srcDir
            val manifestOut = moduleTemplateData.manifestDir
            val instrumentationTestOut = moduleTemplateData.component5()
            val localTestOut = moduleTemplateData.component6()
            val moduleOut = moduleTemplateData.component8()
            val language = projectData.language
            createDirectory(srcOut)
            addIncludeToSettings(moduleTemplateData.name)
            val buildFile = if (useKts) "build.gradle.kts" else "build.gradle"
            save(buildGradle(moduleTemplateData.namespace), moduleOut.resolve(buildFile))
            requireJavaVersion(bytecodeLevel.versionString, moduleTemplateData.projectTemplateData.language == Language.Kotlin)
            save(generateManifest(), manifestOut.resolve("AndroidManifest.xml"))
            save(gitignore(), moduleOut.resolve(".gitignore"))
            proguardRecipe(moduleOut, moduleTemplateData.isLibrary)

            if (generateGenericLocalTests) {
                addLocalTests(moduleTemplateData.packageName, localTestOut, language)
                addInstrumentedTests(packageName = moduleTemplateData.packageName,
                        useAndroidX = true,
                        isLibraryProject = true,
                        instrumentedTestOut = instrumentationTestOut,
                        language = language)
            }
            createDirectory(moduleOut.resolve("$srcOut/data"))
            createDirectory(moduleOut.resolve("$srcOut/ui"))
        }

        private fun buildGradle(namespace: String): String {
            val content = javaClass.classLoader.getResourceAsStream(MODULE_GRADLE_FILE_NAME)?.use { inputStream ->
                inputStream.bufferedReader().readText()
            } ?: throw IllegalArgumentException("File not found: $MODULE_GRADLE_FILE_NAME")
            return content.replace("{{namespace}}", namespace)
        }

        private const val MODULE_GRADLE_FILE_NAME = "default-build.gradle"
    }
}