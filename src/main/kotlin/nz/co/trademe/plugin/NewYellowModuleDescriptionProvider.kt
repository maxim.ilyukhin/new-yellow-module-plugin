package nz.co.trademe.plugin

import com.android.tools.idea.npw.model.ProjectSyncInvoker
import com.android.tools.idea.npw.module.ConfigureModuleStep
import com.android.tools.idea.npw.module.ModuleDescriptionProvider
import com.android.tools.idea.npw.module.ModuleGalleryEntry
import com.android.tools.idea.npw.module.ModuleModel
import com.android.tools.idea.npw.toWizardFormFactor
import com.android.tools.idea.wizard.model.SkippableWizardStep
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.DialogPanel
import com.intellij.ui.ContextHelpLabel
import com.intellij.ui.dsl.builder.AlignX
import com.intellij.ui.dsl.builder.panel
import com.intellij.util.ui.JBUI
import icons.StudioIcons.Wizards.Modules
import org.jetbrains.android.util.AndroidBundle
import java.io.File


class NewYellowModuleDescriptionProvider : ModuleDescriptionProvider {
    override fun getDescriptions(project: Project): Collection<ModuleGalleryEntry> {
        return listOf(YellowModuleGalleryEntry())
    }

    companion object {
        const val NAME = "Yellow Module"
        const val MODULE_NAME = "Create a new Yellow app module"
        const val DEFAULT_MIN_SDK = 24
    }

    class YellowModuleGalleryEntry : ModuleGalleryEntry {
        override val description = MODULE_NAME
        override val name = NAME
        override val icon = Modules.ANDROID_LIBRARY

        override fun createStep(project: Project, moduleParent: String, projectSyncInvoker: ProjectSyncInvoker): SkippableWizardStep<ModuleModel> {
            val basePackage = "nz.co.trademe"
            val moduleModel = YellowModuleModel.fromExistingProject(project, moduleParent, projectSyncInvoker)
            val minSdk = getMinSdk(project.basePath) ?: DEFAULT_MIN_SDK
            return ConfigureYellowModuleStep(moduleModel, minSdk, basePackage, this.name)
        }

        private fun getMinSdk(basePath: String?): Int? {
            return basePath?.let {
                val versionsGradleFile = File("$basePath/versions.gradle")

                if (versionsGradleFile.exists()) {
                    val minSdk = versionsGradleFile.readLines().firstOrNull { it.contains("minSdk") }
                    minSdk?.let {
                        val minSdkVersion = minSdk.split("=")[1].trim().toIntOrNull()
                        minSdkVersion
                    }
                } else {
                    null
                }
            }
        }
    }

    class ConfigureYellowModuleStep(
            moduleModel: YellowModuleModel,
            minSdkLevel: Int,
            basePackage: String?,
            title: String,
    ) : ConfigureModuleStep<ModuleModel>(moduleModel, moduleModel.formFactor.get().toWizardFormFactor(), minSdkLevel, basePackage, title) {

        override fun createMainPanel(): DialogPanel {
            return panel {
                val moduleNameHelpText = AndroidBundle.message("android.wizard.module.help.name", *arrayOfNulls(0))
                val moduleNameLabel = ContextHelpLabel.create(moduleNameHelpText).apply {
                    text = "Module name"
                    horizontalTextPosition = 2
                }
                row(moduleNameLabel) {
                    cell(moduleName).align(AlignX.FILL)
                }

                row("Package name") {
                    cell(packageName).align(AlignX.FILL)
                }
            }.withBorder(JBUI.Borders.empty(6))
        }
    }
}
