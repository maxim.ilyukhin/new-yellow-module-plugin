plugins {
    id("java")
    id("org.jetbrains.kotlin.jvm") version "1.9.0"
    id("org.jetbrains.intellij") version "1.15.0"
}

group = "nz.co.trademe.plugin"
version = "0.0.2"

repositories {
    mavenCentral()
}

intellij {
    version.set("2023.1.1.26")
    type.set("AI")

    plugins.set(listOf("android"))
}

tasks {
    // Set the JVM compatibility versions
    withType<JavaCompile> {
        sourceCompatibility = "17"
        targetCompatibility = "17"
    }
    withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions.jvmTarget = "17"
    }

    patchPluginXml {
        sinceBuild.set("231")
        untilBuild.set("232.*")
    }

    signPlugin {
        certificateChain.set(System.getenv("CERTIFICATE_CHAIN"))
        privateKey.set(System.getenv("PRIVATE_KEY"))
        password.set(System.getenv("PRIVATE_KEY_PASSWORD"))
    }

    publishPlugin {
        token.set(System.getenv("PUBLISH_TOKEN"))
    }

    runIde {
        ideDir.set(file("/Applications/Android Studio.app/Contents")) // Mac os specific path
    }
}

tasks.named("buildSearchableOptions") {
    enabled = false
}

tasks.register<Copy>("updateNewModulePlugin") {
    dependsOn("buildPlugin")

    from("build/distributions") {
        include("NewModulePlugin*.zip")
    }
    into(project.rootDir)

    rename { "new-module-plugin.zip" }

    doFirst {
        println("Copying NewModulePlugin zip file to root directory...")
    }

    doLast {
        println("Update of new-module-plugin.zip complete.")
    }
}

tasks.named("buildPlugin").configure {
    finalizedBy("updateNewModulePlugin")
}
