## Android Studio plugin to create a new module in Yellow app

This plugin is used to create a new module in Yellow app. It will create a new module project specific gradle file

To install it in Android Studio, go to `Settings` -> `Plugins` -> `Install Plugin from Disk...` and select the `NewModulePlugin-{version}.zip` file.

To use it, right click on the `app` module and select `New Module...`. Then select `Yellow Module` and enter the name of the module.
The plugin will create a new module with the name you entered and add it to the `settings.gradle` file.

## How to build
To generate the zip file, run the following command:
``` bash
./gradlew buildPlugin
```
The zip file will be generated in the `build/distributions` folder.

Additional information about intellij plugin development can be found [here](https://plugins.jetbrains.com/docs/intellij/welcome.html)
Android Studio releases list can be found [here](https://plugins.jetbrains.com/docs/intellij/android-studio-releases-list.html)

